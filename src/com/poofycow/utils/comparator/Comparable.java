package com.poofycow.utils.comparator;

public interface Comparable {
    public boolean compare(Comparable b);
}
